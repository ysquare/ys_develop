<?php

namespace YSquare\Develop\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;

class ViewConfig extends ViewConfig_parent
{
    public function ysClearTmp()
    {
        $sFnc = Registry::get(Request::class)->getRequestParameter('ys_clear_tmp');

        if ($sFnc == 'cleartpl') {
            $this->clearTpl();
        } elseif ($sFnc == 'cleartmp') {
            $this->clearTmp();
        }
    }

    private function clearPattern($pattern)
    {
        foreach (glob($pattern) as $item) {
            if (is_file($item)) {
                unlink($item);
            }
        }
    }

    private function clearTmp()
    {
        $pattern = Registry::getConfig()->getConfigParam("sCompileDir") . "/*.txt";
        $this->clearPattern($pattern);
    }

    private function clearTpl()
    {
        $directory = Registry::getConfig()->getConfigParam("sCompileDir") . "template_cache/";
        $pattern = '/.*\.php$/i';
        $this->recursiveClearByPattern($directory, $pattern);

        $pattern = Registry::getConfig()->getConfigParam("sCompileDir") . "smarty/*.php";
        $this->clearPattern($pattern);
    }

    private function recursiveClearByPattern($baseDir, $pattern){
        $dir = new \RecursiveDirectoryIterator(
            $baseDir,
            \RecursiveDirectoryIterator::KEY_AS_FILENAME |
            \RecursiveDirectoryIterator::CURRENT_AS_FILEINFO
        );
        $ite = new \RecursiveIteratorIterator($dir);
        $files = new \RegexIterator($ite, $pattern, \RegexIterator::MATCH, \RegexIterator::USE_KEY);
        foreach ($files as $file) {
//            $logger = Registry::getLogger();
//            $logger->error("-> Delete: " . $file->getPathName());
//            $this->log("-> Delete: " . $file->getPathName());
            unlink($file->getPathName());
        }

    }
}
