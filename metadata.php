<?php

use OxidEsales\Eshop\Core\ViewConfig;

/**
 * Metadata version
 */
$sMetadataVersion = '2.1';

/**
 * Module information
 */
$aModule = [
    'id'          => 'ys_develop',
    'title'       => '[Y-SQUARE] Develop Helper',
    'description' => [
        'de' => 'Enthält eine Sammlung von kleinen Hilfen für die Entwicklung',
        'en' => 'Includes helper functions for development purposes',
    ],
    'thumbnail'   => 'logo.png',
    'version'     => '2.0.0',
    'author'      => 'Y-SQUARE',
    'url'         => 'https://www.y-square.com',
    'email'       => 'info@y-square.com',
    'extend'      => [
        ViewConfig::class => YSquare\Develop\Core\ViewConfig::class,
    ],
    'controllers' => [
        'ys_develop__admin_variables' => YSquare\Develop\Controller\Admin\VariablesController::class,
    ],
    'templates'   => [
//        'ys_develop__admin_variables.tpl' => 'ysquare/ys_develop/views/admin/tpl/ys_develop__admin_variables.tpl',
    ],
    'blocks'      => [
//        [
//            'template' => 'include/header_links.tpl',
//            'block'    => 'admin_header_links',
//            'file'     => 'views/admin/tpl/header.tpl',
//        ],
    ],
    'settings'    => [],
    'events'      => [
    ],
];