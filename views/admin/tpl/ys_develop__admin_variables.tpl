<div>
    [{$oView->getDefaultVariables()}]<br>--<br>
    [{assign var="language" value=$oView->getLanguageSettings()}]

    <pre>[{$language.params|@var_dump}]<br>--<br>
    [{$language.lang|@var_dump}]<br>--<br>
    [{$language.urls|@var_dump}]<br>--<br>
    [{$language.sslUrls|@var_dump}]<br>--<br>
    </pre>
</div>