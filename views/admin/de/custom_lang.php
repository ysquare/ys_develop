
<?php

$sLangName = "Deutsch";

$aLang = [
    'charset' => 'UTF-8',

    'ys_admin_dev_menugroup' => "[Y] Dev Tools",
    'ys_dev_check' => 'Prüfung',
    'ys_dev_check_variables' => 'Variablen',


    'DEVUTIL_CLEARTMP_LABEL' => 'tmp-Ordner leeren',
    'DEVUTIL_CLEARTPL_LABEL' => 'Smarty-Cache leeren',
];