<?php

namespace YSquare\Develop\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;

class VariablesController extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController
{
    protected $_sThisTemplate = '@ys_develop/admin_twig/variables.html.twig';

    public function getDefaultVariables()
    {
        $key = Registry::getConfig()->getConfigParam('sConfigKey');
        return $key;
    }

    public function getLanguageSettings()
    {
        $aLangData['params'] = Registry::getConfig()->getConfigParam('aLanguageParams');
        $aLangData['lang'] = Registry::getConfig()->getConfigParam('aLanguages');
        $aLangData['urls'] = Registry::getConfig()->getConfigParam('aLanguageURLs');
        $aLangData['sslUrls'] = Registry::getConfig()->getConfigParam('aLanguageSSLURLs');
        return $aLangData;
    }
}